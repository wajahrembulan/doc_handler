/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     05/07/2017 15:32:09                          */
/*==============================================================*/

create database doc_handler;
use doc_handler;
/*==============================================================*/
/* Table: ADMIN                                                 */
/*==============================================================*/
create table ADMIN
(
   USERNAME             char(30) not null,
   PASSWORD             char(30),
   primary key (USERNAME)
);

/*==============================================================*/
/* Table: DOKUMEN                                               */
/*==============================================================*/
create table DOKUMEN
(
   ID                   int not null,
   NOMOR                int not null,
   PENGIRIM             varchar(255),
   TANGGALTERIMA        date,
   NOMORDOKUMEN         varchar(50),
   TANGGAL              date,
   PERIHAL              text,
   KETERANGAN           text,
   primary key (ID, NOMOR)
);

/*==============================================================*/
/* Table: KLASIFIKASI_DOKUMEN                                   */
/*==============================================================*/
create table KLASIFIKASI_DOKUMEN
(
   ID                   int not null,
   NAMA_KLASIFIKASI     char(100),
   primary key (ID)
);

alter table DOKUMEN add constraint FK_RELATIONSHIP_1 foreign key (ID)
      references KLASIFIKASI_DOKUMEN (ID) on delete restrict on update restrict;
insert into admin values ('admin','admin');
insert into klasifikasi_dokumen values (1,'t1'),
(2,'t2'),
(3,'t3'),
(4,'arsip');


insert into dokumen values (4,1,'pengirim 1','2009-03-24','nodok 1','2000-05-25','perihal 1','keterangan 1'),
(4,2,'pengirim 2','2006-09-25','nodok 2','2006-07-02','perihal 2','keterangan 2'),
(4,3,'pengirim 3','2008-07-29','nodok 3','2003-12-12','perihal 3','keterangan 3'),
(4,4,'pengirim 4','2003-11-17','nodok 4','2011-09-13','perihal 4','keterangan 4'),
(4,5,'pengirim 5','2011-10-16','nodok 5','2013-03-25','perihal 5','keterangan 5'),
(4,6,'pengirim 6','2014-01-01','nodok 6','2009-05-11','perihal 6','keterangan 6'),
(4,7,'pengirim 7','2015-08-04','nodok 7','2007-02-16','perihal 7','keterangan 7'),
(4,8,'pengirim 8','2016-04-09','nodok 8','2007-01-31','perihal 8','keterangan 8'),
(4,9,'pengirim 9','2000-09-22','nodok 9','2003-06-25','perihal 9','keterangan 9'),
(4,10,'pengirim 10','2004-04-25','nodok 10','2004-10-22','perihal 10','keterangan 10'),
(4,11,'pengirim 11','2010-12-28','nodok 11','2005-02-09','perihal 11','keterangan 11'),
(4,12,'pengirim 12','2006-09-04','nodok 12','2002-02-25','perihal 12','keterangan 12'),
(4,13,'pengirim 13','2003-02-28','nodok 13','2004-06-01','perihal 13','keterangan 13'),
(4,14,'pengirim 14','2016-04-16','nodok 14','2001-05-11','perihal 14','keterangan 14'),
(4,15,'pengirim 15','2001-02-20','nodok 15','2010-10-13','perihal 15','keterangan 15'),
(4,16,'pengirim 16','2005-05-08','nodok 16','2008-04-15','perihal 16','keterangan 16'),
(4,17,'pengirim 17','2003-05-30','nodok 17','2008-09-04','perihal 17','keterangan 17'),
(4,18,'pengirim 18','2008-03-27','nodok 18','2011-06-05','perihal 18','keterangan 18'),
(4,19,'pengirim 19','2011-12-23','nodok 19','2002-08-02','perihal 19','keterangan 19'),
(4,20,'pengirim 20','2008-10-11','nodok 20','2016-08-26','perihal 20','keterangan 20'),
(4,21,'pengirim 21','2014-01-13','nodok 21','2007-10-21','perihal 21','keterangan 21'),
(4,22,'pengirim 22','2005-10-27','nodok 22','2011-06-27','perihal 22','keterangan 22'),
(4,23,'pengirim 23','2005-11-26','nodok 23','2001-10-06','perihal 23','keterangan 23'),
(4,24,'pengirim 24','2004-10-07','nodok 24','2013-02-16','perihal 24','keterangan 24'),
(4,25,'pengirim 25','2006-05-24','nodok 25','2013-10-09','perihal 25','keterangan 25');


insert into dokumen values (1,1,'pengirim 1','2009-03-24','nodok 1','2000-05-25','perihal 1','keterangan 1'),
(1,2,'pengirim 2','2006-09-25','nodok 2','2006-07-02','perihal 2','keterangan 2'),
(1,3,'pengirim 3','2008-07-29','nodok 3','2003-12-12','perihal 3','keterangan 3'),
(1,4,'pengirim 4','2003-11-17','nodok 4','2011-09-13','perihal 4','keterangan 4'),
(1,5,'pengirim 5','2011-10-16','nodok 5','2013-03-25','perihal 5','keterangan 5'),
(1,6,'pengirim 6','2014-01-01','nodok 6','2009-05-11','perihal 6','keterangan 6'),
(1,7,'pengirim 7','2015-08-04','nodok 7','2007-02-16','perihal 7','keterangan 7'),
(1,8,'pengirim 8','2016-04-09','nodok 8','2007-01-31','perihal 8','keterangan 8'),
(1,9,'pengirim 9','2000-09-22','nodok 9','2003-06-25','perihal 9','keterangan 9'),
(1,10,'pengirim 10','2004-04-25','nodok 10','2004-10-22','perihal 10','keterangan 10'),
(1,11,'pengirim 11','2010-12-28','nodok 11','2005-02-09','perihal 11','keterangan 11'),
(1,12,'pengirim 12','2006-09-04','nodok 12','2002-02-25','perihal 12','keterangan 12'),
(1,13,'pengirim 13','2003-02-28','nodok 13','2004-06-01','perihal 13','keterangan 13'),
(1,14,'pengirim 14','2016-04-16','nodok 14','2001-05-11','perihal 14','keterangan 14'),
(1,15,'pengirim 15','2001-02-20','nodok 15','2010-10-13','perihal 15','keterangan 15'),
(1,16,'pengirim 16','2005-05-08','nodok 16','2008-04-15','perihal 16','keterangan 16'),
(1,17,'pengirim 17','2003-05-30','nodok 17','2008-09-04','perihal 17','keterangan 17'),
(1,18,'pengirim 18','2008-03-27','nodok 18','2011-06-05','perihal 18','keterangan 18'),
(1,19,'pengirim 19','2011-12-23','nodok 19','2002-08-02','perihal 19','keterangan 19'),
(1,20,'pengirim 20','2008-10-11','nodok 20','2016-08-26','perihal 20','keterangan 20'),
(1,21,'pengirim 21','2014-01-13','nodok 21','2007-10-21','perihal 21','keterangan 21'),
(1,22,'pengirim 22','2005-10-27','nodok 22','2011-06-27','perihal 22','keterangan 22'),
(1,23,'pengirim 23','2005-11-26','nodok 23','2001-10-06','perihal 23','keterangan 23'),
(1,24,'pengirim 24','2004-10-07','nodok 24','2013-02-16','perihal 24','keterangan 24'),
(1,25,'pengirim 25','2006-05-24','nodok 25','2013-10-09','perihal 25','keterangan 25');

insert into dokumen values (2,1,'pengirim 1','2009-03-24','nodok 1','2000-05-25','perihal 1','keterangan 1'),
(2,2,'pengirim 2','2006-09-25','nodok 2','2006-07-02','perihal 2','keterangan 2'),
(2,3,'pengirim 3','2008-07-29','nodok 3','2003-12-12','perihal 3','keterangan 3'),
(2,4,'pengirim 4','2003-11-17','nodok 4','2011-09-13','perihal 4','keterangan 4'),
(2,5,'pengirim 5','2011-10-16','nodok 5','2013-03-25','perihal 5','keterangan 5'),
(2,6,'pengirim 6','2014-01-01','nodok 6','2009-05-11','perihal 6','keterangan 6'),
(2,7,'pengirim 7','2015-08-04','nodok 7','2007-02-16','perihal 7','keterangan 7'),
(2,8,'pengirim 8','2016-04-09','nodok 8','2007-01-31','perihal 8','keterangan 8'),
(2,9,'pengirim 9','2000-09-22','nodok 9','2003-06-25','perihal 9','keterangan 9'),
(2,10,'pengirim 10','2004-04-25','nodok 10','2004-10-22','perihal 10','keterangan 10'),
(2,11,'pengirim 11','2010-12-28','nodok 11','2005-02-09','perihal 11','keterangan 11'),
(2,12,'pengirim 12','2006-09-04','nodok 12','2002-02-25','perihal 12','keterangan 12'),
(2,13,'pengirim 13','2003-02-28','nodok 13','2004-06-01','perihal 13','keterangan 13'),
(2,14,'pengirim 14','2016-04-16','nodok 14','2001-05-11','perihal 14','keterangan 14'),
(2,15,'pengirim 15','2001-02-20','nodok 15','2010-10-13','perihal 15','keterangan 15'),
(2,16,'pengirim 16','2005-05-08','nodok 16','2008-04-15','perihal 16','keterangan 16'),
(2,17,'pengirim 17','2003-05-30','nodok 17','2008-09-04','perihal 17','keterangan 17'),
(2,18,'pengirim 18','2008-03-27','nodok 18','2011-06-05','perihal 18','keterangan 18'),
(2,19,'pengirim 19','2011-12-23','nodok 19','2002-08-02','perihal 19','keterangan 19'),
(2,20,'pengirim 20','2008-10-11','nodok 20','2016-08-26','perihal 20','keterangan 20'),
(2,21,'pengirim 21','2014-01-13','nodok 21','2007-10-21','perihal 21','keterangan 21'),
(2,22,'pengirim 22','2005-10-27','nodok 22','2011-06-27','perihal 22','keterangan 22'),
(2,23,'pengirim 23','2005-11-26','nodok 23','2001-10-06','perihal 23','keterangan 23'),
(2,24,'pengirim 24','2004-10-07','nodok 24','2013-02-16','perihal 24','keterangan 24'),
(2,25,'pengirim 25','2006-05-24','nodok 25','2013-10-09','perihal 25','keterangan 25');


insert into dokumen values (3,1,'pengirim 1','2009-03-24','nodok 1','2000-05-25','perihal 1','keterangan 1'),
(3,2,'pengirim 2','2006-09-25','nodok 2','2006-07-02','perihal 2','keterangan 2'),
(3,3,'pengirim 3','2008-07-29','nodok 3','2003-12-12','perihal 3','keterangan 3'),
(3,4,'pengirim 4','2003-11-17','nodok 4','2011-09-13','perihal 4','keterangan 4'),
(3,5,'pengirim 5','2011-10-16','nodok 5','2013-03-25','perihal 5','keterangan 5'),
(3,6,'pengirim 6','2014-01-01','nodok 6','2009-05-11','perihal 6','keterangan 6'),
(3,7,'pengirim 7','2015-08-04','nodok 7','2007-02-16','perihal 7','keterangan 7'),
(3,8,'pengirim 8','2016-04-09','nodok 8','2007-01-31','perihal 8','keterangan 8'),
(3,9,'pengirim 9','2000-09-22','nodok 9','2003-06-25','perihal 9','keterangan 9'),
(3,10,'pengirim 10','2004-04-25','nodok 10','2004-10-22','perihal 10','keterangan 10'),
(3,11,'pengirim 11','2010-12-28','nodok 11','2005-02-09','perihal 11','keterangan 11'),
(3,12,'pengirim 12','2006-09-04','nodok 12','2002-02-25','perihal 12','keterangan 12'),
(3,13,'pengirim 13','2003-02-28','nodok 13','2004-06-01','perihal 13','keterangan 13'),
(3,14,'pengirim 14','2016-04-16','nodok 14','2001-05-11','perihal 14','keterangan 14'),
(3,15,'pengirim 15','2001-02-20','nodok 15','2010-10-13','perihal 15','keterangan 15'),
(3,16,'pengirim 16','2005-05-08','nodok 16','2008-04-15','perihal 16','keterangan 16'),
(3,17,'pengirim 17','2003-05-30','nodok 17','2008-09-04','perihal 17','keterangan 17'),
(3,18,'pengirim 18','2008-03-27','nodok 18','2011-06-05','perihal 18','keterangan 18'),
(3,19,'pengirim 19','2011-12-23','nodok 19','2002-08-02','perihal 19','keterangan 19'),
(3,20,'pengirim 20','2008-10-11','nodok 20','2016-08-26','perihal 20','keterangan 20'),
(3,21,'pengirim 21','2014-01-13','nodok 21','2007-10-21','perihal 21','keterangan 21'),
(3,22,'pengirim 22','2005-10-27','nodok 22','2011-06-27','perihal 22','keterangan 22'),
(3,23,'pengirim 23','2005-11-26','nodok 23','2001-10-06','perihal 23','keterangan 23'),
(3,24,'pengirim 24','2004-10-07','nodok 24','2013-02-16','perihal 24','keterangan 24'),
(3,25,'pengirim 25','2006-05-24','nodok 25','2013-10-09','perihal 25','keterangan 25');