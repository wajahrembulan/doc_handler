<?php
session_start();
$jumlah=20;
$conn = new mysqli("localhost", "root", "", "doc_handler");
$sql = "select * from dokumen order by nomor desc";
$result = $conn->query($sql);
$n= $result->fetch_assoc();
if(isset($_GET['id'])){
	$sql = "SELECT * FROM dokumen where id=".$_GET['id'];
}else
	header('location:login.php');
$result = $conn->query($sql);
?>

<!DOCTYPE html>
<html>
<head>
	 <link rel="stylesheet" href="ulli.css">
	 <link rel="stylesheet" href="table.css">
	 <link rel="stylesheet" href="html.css">
	 <link rel="stylesheet" href="buttonbox.css">
	 <link rel="stylesheet" href="pagination.css">

	 <title>Halaman Utama</title>
</head>

<style>
.active {
	background-color: #944107;
	color: white;
}	
</style>

<body>


<ul>
	<li> <a href="home.php">HOME</a> </li>	
	<li> <a class="<?php if(isset($_GET['id']) && $_GET['id'] == "1") echo "active";?>" href="index.php?id=1">T1</a> </li>	
	<li> <a class="<?php if(isset($_GET['id']) && $_GET['id'] == "2") echo "active";?>" href="index.php?id=2">T2</a> </li>	
	<li> <a class="<?php if(isset($_GET['id']) && $_GET['id'] == "3") echo "active";?>" href="index.php?id=3">T3</a> </li>
	<li> <a class="<?php if(isset($_GET['id']) && $_GET['id'] == "4") echo "active";?>" href="index.php?id=4">ARSIP</a> </li>
	<li><a href="tambahdokumen.php?no=<?php echo $n['NOMOR']+1; ?>" style="<?php if(!isset($_SESSION['username'])) echo "display:none"; ?>">TAMBAH DOKUMEN</a></li>
	<li><a href="ubahpassword.php" style="<?php if(!isset($_SESSION['username'])) echo "display:none"; ?>">UBAH PASSWORD</a></li>
	<li style="float:right;<?php if(isset($_SESSION['username'])) echo "display:none"; ?>"> <a href="login.php">LOGIN AS ADMIN</a> </li>
	<li style="float:right;<?php if(!isset($_SESSION['username'])) echo "display:none"; ?>"> <a href="logout.php">LOGOUT</a> </li>
</ul>


<br>
<br>
<br>

<center>
<table>
	<tr> 
		<th align="center">NO</th>
		<th align="center">PENGIRIM</th>
		<th align="center">TANGGAL INPUT</th>
		<th align="center">NOMOR DOKUMEN</th>
		<th align="center">TANGGAL</th>
		<th align="center">PERIHAL</th>
		<th align="center">AKSI</th>
	</tr>
	<?php
	if($result->num_rows>0){
		for($i=1;$i<=$result->num_rows;$i++){
			$row = $result->fetch_assoc();
			if(isset($_GET["p"]))
				$page=$_GET["p"];
			else
				$page=1;
			$temp=($page-1)*$jumlah;
			if($i+1<=$temp)
				continue;
			if($i>$page*$jumlah)
				continue;
	?>
	<tr>
		<td align="center"> <?php echo $i;?></td>
		<td align="center"> <?php echo $row["PENGIRIM"];?></td>
		<td align="center"> <?php echo $row["TANGGALTERIMA"];?></td>
		<td align="center"> <?php echo $row["NOMORDOKUMEN"];?></td>
		<td align="center"> <?php echo $row["TANGGAL"];?></td>
		<td align="center"> <?php echo $row["PERIHAL"];?></td>
		<td align="center">
			<a href="detaildokumen.php?no=<?php echo $row['NOMOR']; ?>" class="button-table-print">LIHAT</a>
			<a style="<?php if(!isset($_SESSION['username'])) echo "display:none"; ?>" href="editdokumen.php?no=<?php echo $row['NOMOR']; ?>" class="button-table-edit">EDIT</a>
			<a style="<?php if(!isset($_SESSION['username'])) echo "display:none"; ?>" href="hapusdokumen.php?no=<?php echo $row['NOMOR']; ?>" class="button-table-hapus">HAPUS</a>
		</td>
	</tr>	
	<?php
		}
	}
	?>
</table>

<br>


<div class="pagination">

	<?php
		if($result->num_rows>0){
		for($ii=1;$ii<=ceil(($i-1)/$jumlah);$ii++){
	?>
		<a class="<?php if(isset($_GET['p']) && $_GET['p'] == $ii) echo "active"; else echo ""; ?>" href="index.php?id=<?php if(isset($_GET['id'])) echo $_GET['id']; else echo "4"; ?>&p=<?php echo $ii;?>"><?php echo $ii;?></a>
	<?php
		}}
	?>


</div>

</div>


<br>
<br>


<a target="_blank" href="printdokumen.php?id=<?php if(isset($_GET['id'])) echo $_GET['id'];else echo "4"; ?>&p=<?php if(isset($_GET['p'])) echo $_GET['p'];else echo "1"; ?>">
<!-- printicon -->
<img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDE2IDE2IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxNiAxNjsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIxNnB4IiBoZWlnaHQ9IjE2cHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik00LDEyLjV2M2g4di0zdi0ySDRWMTIuNXogTTUsMTEuNWg2djFINVYxMS41eiBNNSwxMy41aDZ2MUg1VjEzLjV6IiBmaWxsPSIjMDAwMDAwIi8+CgkJPHBvbHlnb24gcG9pbnRzPSIxMiwzLjUgMTIsMC41IDQsMC41IDQsMy41IDQsNS41IDEyLDUuNSAgICIgZmlsbD0iIzAwMDAwMCIvPgoJCTxwYXRoIGQ9Ik0xNCwzLjVoLTF2MnYxSDN2LTF2LTJIMmMtMSwwLTIsMS0yLDJ2NWMwLDEsMSwyLDIsMmgxdi0ydi0xaDEwdjF2MmgxYzEsMCwyLTEsMi0ydi01ICAgIEMxNiw0LjUsMTUsMy41LDE0LDMuNXoiIGZpbGw9IiMwMDAwMDAiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" />
<!-- printicon -->
</a>


<div class="footer"><b>&copy; 2017</b></div>
</body>
</html>